import processing.pdf.*;
/*-------------------------------------------ellipseDoorelkaar--------------------------------------*/
//Esat Ergunes 
//esat.ergunes@student.ehb.be
//2017-2018
//MULTEC
/*--------------------------------------------------------------------------------------------------*/

float max_afstand;
PFont font1;//font class om font te creëren
void setup() {
  size(600, 800, PDF, "CP1_Taak1_EsatErgünes.pdf"); 
  noStroke();
  colorMode(HSB, 360, 100, 100);
  background(0);
  max_afstand = dist(width/1, height/4, width, height);//afstand tussen twee punten
  drawcirkles();
  addText();
}

/*----------------------------------code voor cirkels----------------------------------------------*/

void drawcirkles() {
  /*cirkels tekenen in loop Breedte*/  for (int i = 0; i <= width; i += 10*2) {
    /*cirkels tekenen in loop Hoogte*/    for (int j = 0; j <= height; j += 10*2) {
      float size = dist(width+500, height+500, width/2, height/2);//ruimte tussen de cirkles
      size = size / max_afstand*20;
      fill(random(0, 55), random(100), 100, random(100));
      ellipse(i, j, size*1, size*1);
    }
  }
}

/*-------------------------------------code voor Tekst----------------------------------------------*/
void addText() {
  fill(0, 0, 255); 
  textAlign(RIGHT);
  text("Ergünes Esat Multec Student2017-2018 esat.ergunes@student.ehb.be", width-190, height-55, 175, 300);
 font1 = loadFont("HelveticaNeue-Bold-48.vlw");
}